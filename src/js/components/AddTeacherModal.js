export default (onAdd, openButtonNode, parentNode) => {
  const show = () => parentNode.classList.remove('popup-hidden');
  const hide = () => parentNode.classList.add('popup-hidden');
  openButtonNode.addEventListener('click', show);
  parentNode.querySelector('.popup-close').addEventListener('click', hide);
  parentNode.querySelector('#add-teacher-button').addEventListener('click', (e) => {
    e.preventDefault();
    const value = [...parentNode.getElementsByTagName('input'), ...parentNode.getElementsByTagName('select')].filter((n) => (n.type === 'radio' ? n.checked : true)).reduce((acc, curr) => ({ ...acc, [curr.name]: curr.value }), {});
    onAdd({
      ...value,
      age: +value.age,
      title: value.gender === 'male' ? 'Mr' : 'Ms',
      picture_large: 'https://randomuser.me/api/portraits/men/28.jpg',
      picture_thumbnail: 'https://randomuser.me/api/portraits/thumb/men/28.jpg',
      note: 'Melted cheese jarlsberg queso. Fromage danish fontina roquefort cut the cheese parmesan jarlsberg st. agur blue cheese cream cheese. Cheese strings pecorino roquefort pecorino lancashire caerphilly manchego cheese triangles. Halloumi when the cheese comes out everybody`s happy cheeseburger taleggio jarlsberg cut the cheese cheesy grin stilton. Babybel taleggio goat.',
    });
    hide();
  });
  return { show, hide };
};
