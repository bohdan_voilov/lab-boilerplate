import { removeChildren } from '../lib/component';
import TeacherCard from './TeacherCard';

export default (teachers, parent) => {
  removeChildren(parent);
  const cards = teachers.filter((t) => t.favorite).map((t, i) => TeacherCard(i, t));
  cards.forEach((c) => parent.appendChild(c));
};
