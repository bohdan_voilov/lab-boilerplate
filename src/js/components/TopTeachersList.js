import { removeChildren } from '../lib/component';
import TeacherCard from './TeacherCard';

export default (teachers, onClick, parent) => {
  const cards = teachers.map((t, i) => TeacherCard(i, t));
  removeChildren(parent);
  cards.map((c) => parent.appendChild(c));
  [...parent.getElementsByClassName('top-teachers-list-card')].map((t) => t.addEventListener('click', () => onClick(teachers[+t.getAttribute('key')])));
};
