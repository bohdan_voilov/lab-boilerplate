import { sortObjects } from '../lab-3';
import { fromHtml, removeChildren } from '../lib/component';

const tbody = (teachers, parent) => {
  removeChildren(parent);
  const html = teachers.map((t) => `
  <tr>
    <td>${t.fullName}</td>
    <td>${t.age}</td>
    <td>${t.gender}</td>
    <td>${t.country}</td>
  </tr>
  `).join('');
  parent.appendChild(fromHtml(html));
};
export default (teachers, parent) => {
  const tbodyNode = parent.querySelector('tbody');
  tbody(teachers, tbodyNode);
  const theadNode = parent.querySelector('thead');
  const html = `
  <th key="fullName">Name</th>
  <th key="age">Age</th>
  <th key="gender">Gender</th>
  <th key="country">Country</th>
  `;
  theadNode.appendChild(fromHtml(html));
  const ths = [...theadNode.getElementsByTagName('th')];
  let state = [];
  ths.forEach((el) => {
    el.addEventListener('click', () => {
      let value;
      if (el.classList.contains('sort-down')) {
        el.classList.remove('sort-down');
        el.classList.add('sort-up');
        value = true;
      } else if (el.classList.contains('sort-up')) {
        el.classList.remove('sort-up');
        value = null;
      } else {
        el.classList.add('sort-down');
        value = false;
      }
      const key = el.getAttribute('key');
      state = [...state.filter((e) => e.key !== key)];
      if (value != null) {
        state.push({ key, value });
      }
      console.log(state);
      tbody(sortObjects(teachers, state), tbodyNode);
    });
  });
};
