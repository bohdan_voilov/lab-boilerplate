import { fromHtml } from '../lib/component';

export default (key, teacher) => fromHtml(`<div key="${key}" class="top-teachers-list-card">
<img class="top-teachers-list-card-avatar" src="${teacher.picture_thumbnail}"
  alt="Teacher's photo">
${teacher.favorite ? '<img class="top-teachers-list-card-icon" src="images/star.png" alt="Favourite" />' : ''}
<p class="top-teachers-list-card-name">${teacher.fullName.replace(' ', '<br /> ')}</p>
<p class="top-teachers-list-card-country">${teacher.country}</p>
</div>`);
