import { fromHtml, removeChildren } from '../lib/component';

const Select = (name, values, onChange) => {
  const node = fromHtml(`
  <label class="top-teachers-filter-input">
      ${name}
      <select name="${name}" id="select-${name}">
        ${['all', ...values]
    .map((v) => `<option value="${v}">${v}</option>`)}
      </select>
    </label>
  `);
  const select = node.querySelector('select');
  select.addEventListener('change', () => {
    onChange(select.value);
  });
  return node;
};
export default (teachers, filterNames, onChange, parent) => {
  removeChildren(parent);
  const filters = filterNames
    .map((f) => ({ name: f, values: [...new Set(teachers.map((t) => t[f]))] }));
  const getValues = () => [...parent.getElementsByTagName('select')].map((node) => ({ name: node.name, value: node.value }));
  filters
    .map((f) => Select(f.name, f.values, () => onChange(getValues())))
    .map((n) => parent.appendChild(n));
};
