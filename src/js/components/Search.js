export default (teachers, onSearch, inputNode, buttonNode) => {
  buttonNode.addEventListener('click', (e) => {
    e.preventDefault();
    onSearch(inputNode.value);
  });
};
