import { fromHtml, removeChildren } from '../lib/component';

export default (onRemoveFromFavorites, node) => {
  const html = (teacher) => `
  <button class="popup-close"><img src="images/close.png" /></button>
      <h2 class="h2">Teacher Info</h2>
      <section class="teacher-info-section">
         <img class="teacher-info-section-avatar"
            src="${teacher.picture_large}" />
         <div class="teacher-info-section-summary">
            <div>
               ${teacher.favorite ? `<img class="teacher-info-section-summary-icon" src="https://img.icons8.com/fluency/48/000000/star.png"
                  alt="Favourite" />` : ''}
               <h4 class="teacher-info-section-summary-name">${teacher.fullName}</h4>
            </div>
            <address class="teacher-info-section-summary-contacts">
               <p><span class="city">${teacher.city}</span>, <span class="country">${teacher.country}</span></p>
               <p><span class="age">${teacher.age}</span>, <span class="gender">${teacher.gender}</span>
               </p>
               <a href="mailto:${teacher.email}">
                  ${teacher.email}
               </a>
               <p class="phone">${teacher.phone}</p>
            </address>
            <button class="teacher-info-toggle-favorite">${teacher.favorite ? 'Remove from favorites' : 'Add to favorites'}</button>
         </div>
      </section>

      <p class="teacher-info-comment">
         ${teacher.note}
      </p>

      <details class="teacher-info-map-link">
         <summary class="teacher-info-map-link-summary">toggle map</summary>Cheese and biscuits taleggio stilton. Cheesy feet cream cheese red leicester
         manchego cheese triangles when the cheese comes out everybody's happy fromage when the cheese comes out
         everybody's happy. Roquefort blue castello cheese and wine the big cheese bocconcini cheese triangles manchego
         st. agur blue cheese. Croque monsieur croque monsieur queso edam bavarian bergkase lancashire pepper jack
         everyone loves. Cheese on toast lancashire.
      </details>`;
  const hide = () => {
    node.classList.add('popup-hidden');
    removeChildren(node);
  };
  return {
    hide,
    show: (teacher) => {
      node.classList.remove('popup-hidden');
      node.appendChild(fromHtml(html(teacher)));
      node.querySelector('.popup-close').addEventListener('click', () => hide());
      node.querySelector('.teacher-info-toggle-favorite').addEventListener('click', () => onRemoveFromFavorites({ ...teacher, favorite: !teacher.favorite }));
    },
  };
};
