import '../scss/style.scss';
import TeacherRows from './components/TeacherRows';
import TopTeachersList from './components/TopTeachersList';
import TeacherInfoModal from './components/TeacherInfoModal';
import { teachers } from './lab-3';
import FavoriteTeachers from './components/FavoriteTeachers';
import Filters from './components/Filters';
import Search from './components/Search';
import AddTeacherModal from './components/AddTeacherModal';

require('../css/app.css');

/** ******** Your code here! *********** */

// eslint-disable-next-line no-console
console.log({ teachers });
const replaceTeacher = (ts, newT) => ts
  .map((t) => (t.fullName === newT.fullName ? newT : t));
const onFilterChange = (render) => (filterValues) => {
  render(filterValues.reduce((acc, curr) => (curr.value === 'all' ? acc : acc
    .filter((t) => `${t[curr.name]}` === curr.value)), teachers));
};

const render = (ts) => {
  const teacherInfoModal = TeacherInfoModal((t) => {
    teacherInfoModal.hide();
    teacherInfoModal.show(t);
    render(replaceTeacher(ts, t));
  }, document.querySelector('#teacher-info-modal'));
  const onTeacherCardClick = (t) => {
    teacherInfoModal.show(t);
  };
  TopTeachersList(ts, onTeacherCardClick, document.querySelector('#top-teachers-list'));
  FavoriteTeachers(ts, document.querySelector('#favorites > .carousel > .carousel-children'));
};
const filters = () => Filters(teachers, ['country', 'age', 'gender'], onFilterChange(render), document.querySelector('.top-teachers-filter'));
filters();
TeacherRows(teachers, document.querySelector('#statistics'));
const onSearch = (v) => {
  filters();
  const lv = v.toLowerCase();
  render(teachers.filter((t) => (`${t.age}`).includes(lv) || t.note.toLowerCase().includes(lv) || t.fullName.toLowerCase().includes(lv)));
};
Search(teachers, onSearch, document.querySelector('.search > input'), document.querySelector('.search > button'));
const addTeacher = (t) => {
  teachers.push(t);
  console.log(t);
  render(teachers);
};
AddTeacherModal(addTeacher, document.querySelector('#add-teacher-button'), document.querySelector('#add-teacher-modal'));
render(teachers);
