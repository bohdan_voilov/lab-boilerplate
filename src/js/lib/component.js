export const removeChildren = (node) => {
  while (node.firstChild) {
    node.removeChild(node.lastChild);
  }
  return node;
};

export const fromHtml = (html) => {
  const template = document.createElement('template');
  template.innerHTML = html;
  return template.content;
};
